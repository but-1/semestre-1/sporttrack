# README pour l'application SportTrack

## Auteurs
- THEBAULT Claire
- FERRON Sarah

## Objectif de l'application
SportTrack est une application web de suivi d'activités sportives. Elle permet aux utilisateurs de sauvegarder et de gérer des données de position et de fréquence cardiaque à partir de leurs montres "cardio/gps". Les principales fonctionnalités de l'application incluent :
- Page de Connexion
- Page d'Inscription
- Page de Modification d'Informations
- Page de Chargement de Fichiers

## Pages HTML
Ces pages contiennent toutes le même style et la même mise en page avec le bandeau principal du site et la navbar
contenant des liens entre les différentes pages. Les ressources externes utilisées incluent Bootstrap pour la mise en page et le style.

### 1. Page de Connexion (connexion.html)
- Cette page permet aux utilisateurs de se connecter à l'application SportTrack en entrant leur email et leur mot de passe.
- Les types email et mot de passe doivent nécessairement être entrés dans les champs spécfiques.
- En plus de la navbar, un lien est fourni pour rediriger vers la page d'inscription si l'utilisateur n'est pas encore inscrit sur l'application.

### 2. Page d'Inscription (inscription.html)
- Cette page permet aux utilisateurs de s'inscrire sur SportTrack en fournissant diverses informations personnelles.
- Elle contient des champs pour le nom, le prénom, la date de naissance, la taille, le poids, le sexe, l'email et le mot de passe.
- Chaque champ requiert son type spécifique.
- En plus de la navbar, un lien est fourni pour rediriger vers la page de connexion si l'utilisateur est déjà inscrit sur l'application.

### 3. Page de Modification d'Informations (modification.html)
- Cette page permet aux utilisateurs de modifier leurs informations personnelles.
- Elle contient des champs pour le nom, le prénom, la date de naissance, la taille, le poids, le sexe, l'email, le mot de passe actuel et le nouveau mot de passe.
- Chaque champ requiert son type spécifique.

### 4. Page de Chargement de Fichiers (chargementfichier.html)
- Cette page permet aux utilisateurs d'enregistrer des fichiers sportifs.
- Elle propose un bouton pour sélectionner un fichier au format JSON à enregistrer parmi le gestionnaire de fichier de l'utilisateur.

## Installation
Pour installer SportTrack, suivez les étapes ci-dessous :
1) Clonez ce dépôt Git en utilisant la commande suivante : git clone https://gitlab.com/ferron_thebault/SportTrack.git
2) Accédez au répertoire du projet : cd SportTrack.
3) Lancez l'application en utilisant un serveur web de votre choix, avec Apache par exemple.
    - Si vous êtes sous Windows, vous pouvez alors installer Wamp contenant le service Apache qui est le programme permettant de donner accès à
    des pages web. Placez les fichiers dans le répertoir utilisé par Wamp. Si vous êtes sous Linux, vous pouvez installer le package Apache avec la
    commande 'sudo apt apache2', puis démarrer le serveur avec 'sudo service apache2 start'. Les fichiers se trouvent dans le répertoir /var/www/html.
4) Accédez au site dans votre navigateur en utilisant l'URL appropriée.